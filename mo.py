from time import sleep

final_stop = 0.5

def animation(text, init2=None, final2 = None):
    init = 1/len(text) * 0.1 if init2 == None else init2
    final = 0.5 if final2 == None else final2

    t = init
    for char in text:
        print(char,end='', flush =True)
        sleep(t)
    
        t=t*(1.5)

        if t >= final:
            t = init

    print(flush=True)
    sleep(final_stop)

animation("oi amor..")
animation("tudo bem?")
animation("finge que esse código é uma inteligência artificial, fica mais dramático rs..")

animation("gostaria de dizer que você é muito especial para mim..")
animation("a situação está difícil mas logo iremos nos encontrar..")
animation("obrigado por me ensinar muita coisa..")
animation("obrigado por ser anteciosa comigo..")
animation("obrigado por ser minha parceira, por me incentivar nas coisas boas..")
animation("obrigado por mostrar outra forma de ver o mundo, de ver uma forma melhor..")
animation("obrigado por ser tão compreensiva, por me fazer bem quando não estou tanto..")
animation("obrigado por ser muito romântica e ser quem você é..")
animation(".")
animation("...")
animation(".")
animation("eu amo você")
animation(".")
animation(".")
animation(".")
animation(".")


animation(" você é única no mundo ")
animation(" . ")
animation(" . . ")
animation(" . ")


final_stop = 0.0
animation("       você éé                  únicaaa no",final2=0.2)
animation("    você é únicaa             no mundo voce é",final2=0.2)
animation("   você é únicaaa no       mundo você no mundo",final2=0.2)
animation("  você é única no você é única no mundo você é u",final2=0.2)
animation(" você é única no você é única no mundo você é uni",final2=0.2)
animation(" você é única no você é única no mundo você é unic",final2=0.1)
animation("  você é única no você é única no mundo você é un",final2=0.1)
animation("    você é única no você é única no mundo você é ",final2=0.1)
animation("      você é única no você é única no mundo v",final2=0.1)
animation("         você é única no você é única no m",final2=0.1)
animation("            você é única no você é úni",final2=0.1)
animation("               você é única no você",final2=0.1)
animation("                  você é única no",final2=0.1)
animation("                     você é ún",final2=0.1)
animation("                        você",final2=0.1)

final_stop = 0.5
animation(" . ")
animation(" . . ")
animation(" . ")

animation("~seja sempre o que você é~")
animation(" . ")
animation(" .. ")
animation(" . ")
animation("obrigado pelos 7 meses")
animation(" . ")
animation(" . ")
animation("... pera um poquinho..",final2 = 0.6)
animation(" . ")
animation(" ...olha o fibo.. ")


animation("""                     \`*-.                    
                       )  _`-.                 
                      .  : `. .                
                      : _   '  \               
                      ; *` _.   `*-._          
                      `-.-'          `-.       
                        ;       `       `.     
                        :.       .        \    
                        . \  .   :   .-'   .   
                        '  `+.;  ;  '      :   
                        :  '  |    ;       ;-. 
                        ; '   : :`-:     _.`* ;
               [Fibo] .*' /  .*' ; .*`- +'  `* 
                     `*-*   `*-*  `*-*'      """,final2=0.2) 


